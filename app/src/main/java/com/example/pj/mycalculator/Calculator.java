package com.example.pj.mycalculator;

public class Calculator {

    /*
        Калькулятор состоит из двух чисел.
        При получении результата число получает ячейку [0]
    */
    private Double[] numbers = new Double[2];
    /*
        Первоначальное состояние числа в строковой переменной,
        затем оно конвертируется в число
    */
    private String number = "";
    /*
        Переменная операций которая определяет какие отношения
        будут между числами
    */
    private String operation;
    /*
        Переменная есть ли точка!
        Исправляет ошибку в котором можно было ставить больше чем одну точку
    */
    private boolean pointIs = false;

    public Calculator (){

    }
    /*
        Возвращает первоночальный вид числа
    */
    public String getNumber(){
        return number;
    }
    /*
        Возвращает одно из чисел в массиве numbers
    */
    public double getNumbers (int id){
        if (id <= 1 && id >= 0){
            return numbers[id];
        }
        return 0;
    }
    /*
        Возвращает операцию
    */
    public String getOperation() {
        return operation;
    }
    /*
        Добовляет цифру в number
    */
    public void setNumber(char numeral){
        number += numeral;
    }
    /*
        Конвертируем из String в Double
        при operation == "" конвертируется в первое число
        при operation != "" второе число
    */
    public void setNumbers(){
        if (operation == null){
            numbers[0] = Double.parseDouble(number);
            number = "";
        } else if (operation != null){
            numbers[0] = equally();
            number = "";
        }
    }
    /*
        Определяет какая операция должна быть
    */
    public void setOperation (String operation){
        pointIs = false;
        setNumbers();
        this.operation = operation;
    }
    /*
        Метод который в number точку.
        Только ождин раз!
    */
    public void point(){
        if (!pointIs){
            pointIs = true;
            number += ".";
        }
    }
    /*
        Метод который реализует число в проценты
    */
    public Double percent(){
        return equally()/100;
    }
    /*
        Cобытие который происходят после нажатия equally,
        или очередного нажатия на операцию
    */
    public Double equally() {
        if (operation != null && number != ""){
            numbers[1] = Double.parseDouble(number);
            switch (operation){
                case " + ":
                    return numbers[0] + numbers[1];
                case " - ":
                    return numbers[0] - numbers[1];
                case " * ":
                    return numbers[0] * numbers[1];
                case " \\ ":
                    if (numbers[1] != 0) {
                        return numbers[0] / numbers[1];
                    }
                    return 0d;
            }
        } else if (operation == null && number != ""){
            return Double.parseDouble(number);
        }
        return 0d;
    }
    /*
		Полностью обновляет переменные
    */
    public void deletedAll(){
        number = "";
        numbers[0] = null;
        numbers[1] = null;
        operation = null;
    }
    /*
        Удаляет последнюю веденную цифру
    */
    public void deleted() {
        String num = "";
        if (number != "") {
            for (int i = 0; i < number.length() - 1; i++){
                num += number.charAt(i);
            }
        } 
        number = num;
    }
    /*
       Метод переводящий все в строку для визуализации
   */
    @Override
    public String toString(){
        if (operation != null){
            return numbers[0] + operation + number;
        } else if(operation == null){
            return number + "";
        }
        return "";
    }
}
