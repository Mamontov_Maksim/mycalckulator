package com.example.pj.mycalculator;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    //Многопоточность для моментального решения и выведения результата
	final Handler handler = new Handler();
    Runnable r = new Thread(){
        @Override
        public void run(){
            setEqually();
        }
    };

	//Обьект класса реализирующего логику приложения
    Calculator calculator = new Calculator();
    //Переменные кнопок операций
    private Button deletedAll, deleted, equally, point;
    //Переменные кнопок арефметических операций
    private Button division, multiplication, minus, plus, percent;
    //Переменные кнопок чисел
    private Button zero, one, two, three, four, five, six, seven, eight, nine;

    //Создание активности
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createAndAdd();
        text.setText("" + 0);
        setEqually();
    }

    // Слушатель кнопок
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.zero:
                calculator.setNumber('0');
                text.setText(calculator.toString());
                break;
            case R.id.one:
                calculator.setNumber('1');
                text.setText(calculator.toString());
                break;
            case R.id.two:
                calculator.setNumber('2');
                text.setText(calculator.toString());
                break;
            case R.id.three:
                calculator.setNumber('3');
                text.setText(calculator.toString());
                break;
            case R.id.four:
                calculator.setNumber('4');
                text.setText(calculator.toString());
                break;
            case R.id.five:
                calculator.setNumber('5');
                text.setText(calculator.toString());
                break;
            case R.id.six:
                calculator.setNumber('6');
                text.setText(calculator.toString());
                break;
            case R.id.seven:
                calculator.setNumber('7');
                text.setText(calculator.toString());
                break;
            case R.id.eight:
                calculator.setNumber('8');
                text.setText(calculator.toString());
                break;
            case R.id.nine:
                calculator.setNumber('9');
                text.setText(calculator.toString());
                break;
            //Кнопка +
            case R.id.plus:
                calculator.setOperation(" + ");
                text.setText(calculator.toString());
                break;
            // Кнопка -
            case R.id.minus:
                calculator.setOperation(" - ");
                text.setText(calculator.toString());
                break;
            //Кнопка /
            case R.id.division:
                calculator.setOperation(" \\ ");
                text.setText(calculator.toString());
                break;
            // Кнопка *
            case R.id.multiplication:
                calculator.setOperation(" * ");
                text.setText(calculator.toString());
                break;
            //При нажатии процент
            case R.id.percent:
                calculator.percent();
                text.setText(calculator.percent() + "");
                break;
            //при нажатии точки
            case R.id.point:
                calculator.point();
                text.setText(calculator.toString());
                break;
            //При нажатии "="
            case R.id.equally:
                editText.setText("= " + calculator.equally());
                break;
            case R.id.deleted_all:
                calculator.deletedAll();
                text.setText(calculator.toString());
                break;
            case R.id.deleted:
                calculator.deleted();
                text.setText(calculator.toString());
                break;
        }
    }

    //Метод который работает в многопоточном режиме
    public void setEqually() {
        editText.setText("= " + calculator.equally());
        handler.postDelayed(r, 100L);
    }


    // Создает обьекты View и добовляет к слушателю
    public void createAndAdd(){
        text = (TextView) findViewById(R.id.text);
        editText = (EditText) findViewById(R.id.edit_text);
        deletedAll = (Button) findViewById(R.id.deleted_all);
        deleted = (Button) findViewById(R.id.deleted);
        equally = (Button) findViewById(R.id.equally);
        point = (Button) findViewById(R.id.point);
        division = (Button) findViewById(R.id.division);
        multiplication = (Button) findViewById(R.id.multiplication);
        minus = (Button) findViewById(R.id.minus);
        plus = (Button) findViewById(R.id.plus);
        percent = (Button) findViewById(R.id.percent);
        zero = (Button) findViewById(R.id.zero);
        one = (Button) findViewById(R.id.one);
        two = (Button) findViewById(R.id.two);
        three = (Button) findViewById(R.id.three);
        four = (Button) findViewById(R.id.four);
        five = (Button) findViewById(R.id.five);
        six = (Button) findViewById(R.id.six);
        seven = (Button) findViewById(R.id.seven);
        eight = (Button) findViewById(R.id.eight);
        nine = (Button) findViewById(R.id.nine);

        deletedAll.setOnClickListener(this);
        deleted.setOnClickListener(this);
        equally.setOnClickListener(this);
        point.setOnClickListener(this);
        division.setOnClickListener(this);
        multiplication.setOnClickListener(this);
        minus.setOnClickListener(this);
        plus.setOnClickListener(this);
        zero.setOnClickListener(this);
        one.setOnClickListener(this);
        two.setOnClickListener(this);
        three.setOnClickListener(this);
        four.setOnClickListener(this);
        five.setOnClickListener(this);
        six.setOnClickListener(this);
        seven.setOnClickListener(this);
        eight.setOnClickListener(this);
        nine.setOnClickListener(this);
    }
}
